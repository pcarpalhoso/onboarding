package com.bi.camunda.getstarted;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.logging.Logger;

public class ProcessRequestDelegate implements JavaDelegate {

    private final static Logger LOGGER = Logger.getLogger("EMPLOYEE-REQUEST");

    public void execute(DelegateExecution delegateExecution) throws Exception {
        LOGGER.info("Processing request by '" + delegateExecution.getVariable("firstName"));
    }
}
