package com.bi.camunda.getstarted;

import org.camunda.bpm.application.PostDeploy;
import org.camunda.bpm.application.ProcessApplication;
import org.camunda.bpm.application.impl.ServletProcessApplication;
import org.camunda.bpm.engine.FormService;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.runtime.Execution;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.camunda.bpm.engine.variable.Variables;
import org.camunda.bpm.engine.variable.value.ObjectValue;
import org.camunda.bpm.engine.variable.value.StringValue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@ProcessApplication("Onboarding App")
public class OnboardingApplication extends ServletProcessApplication {
    private final static Logger LOGGER = Logger.getLogger("EMPLOYEE-REQUEST");

    @PostDeploy
    public void startProcessInstance(ProcessEngine processEngine) {

        Map<String, Object> vars = Variables.createVariables();
//        Employee employee = new Employee();
//        employee.setFirstName("Alberto");

//        StringValue typedObjectValue = Variables.stringValue(employee.getFirstName());
//                .serializationDataFormat(Variables.SerializationDataFormats.JSON)
//                .create();

//        vars.put("firstName", "Anacleto");
//
//
//
//        RepositoryService repositoryService = processEngine.getRepositoryService();
//
//        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
//                .processDefinitionKey("onboarding")
//                .latestVersion()
//                .singleResult();
//
//        RuntimeService runtimeService = processEngine.getRuntimeService();
//
//        ProcessInstance processInstance = runtimeService.startProcessInstanceById(processDefinition.getId(), vars);


//        FormService formService = processEngine.getFormService();
//        ProcessInstance processInstance = formService.submitStartForm(processDefinition.getId(), variables);

//        LOGGER.info("The process is suspended:".concat(String.valueOf(processDefinition.isSuspended())));
    }

}
