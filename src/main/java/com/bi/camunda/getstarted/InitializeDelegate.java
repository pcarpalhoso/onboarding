package com.bi.camunda.getstarted;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.logging.Logger;

public class InitializeDelegate implements JavaDelegate {

    private Employee employee;
    private final static Logger LOGGER = Logger.getLogger("EMPLOYEE-REQUEST");
    public void execute(DelegateExecution delegateExecution) throws Exception {

        delegateExecution.setVariable("firstName", "Arnaldo");
        LOGGER.info("Inside InitializeDelegate");
    }
}
